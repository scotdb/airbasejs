var express = require('express');
var router = express.Router();
const ibmdb = require('ibm_db');

//ibmdb.open("DATABASE=SAMPLE;HOSTNAME=localhost;UID=db2inst1;PWD=JasnaSk1;PORT=50000;PROTOCOL=TCPIP", 
//  (err,conn) => {
  // function (err,conn) {
//    if (err) return console.log(err);
//  }
//);	
	// res.render('db2test', { title: 'DB2 Test' });

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Add DB2 Test */
router.get('/db2test', function(req,res) {
  ibmdb.open("DATABASE=SAMPLE;HOSTNAME=localhost;UID=db2inst1;PWD=JasnaSk1;PORT=50000;PROTOCOL=TCPIP", 
    (err,conn) => {
    //function (err,conn) {
    if (err) return console.log(err);
    conn.query('SELECT * FROM DEPT', 
      (err,data) => {
      // function (err,data) {
        if (err) console.log(err);
        else {
          console.log(data);
          res.render('db2test', { 
            title: "Db2 Test",
            data: data
          });
        }	
        conn.close(function() {
          console.log('done');
        });
    });
  });	
	// res.render('db2test', { title: 'DB2 Test' });
});	

module.exports = router;
